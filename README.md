## PYCFG

pyton function control flow graph exration as pdf and gv format and show in networkx graph.

## Requirement

- python3

 
## Installation
For install the required packege:

```bash
virtualenv venv
source ./venv/bin/activate  # for linux
.\venv\Scripts\activate   # for windows
pip install -r requirement.txt

sudo apt-get install graphviz  # for linux
install graphviz-2.38.msi from graphviz dirctory   # for windows
```

## Usage

Input of this class is input directory path and python file and function name and output directory which pdf and gv file extract to this path.


```python
from CFG import cfg

example = cfg('./example', "lcm.py", "compute", "./output")
example.extract_pdf()
example.extract_dot()
example.show_graph()
```

## License
Distributed under [MIT](https://gitlab.com/roshan_roshan/pycfg/-/blob/master/LICENSE) license.
