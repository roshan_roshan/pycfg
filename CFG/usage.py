from .tool import show_graph, extract_pdf, extract_dot
import inspect
import sys


class cfg:

    def __init__(self, input_path, input_file, func, output_path):
        self.input_path = input_path
        self.file = input_file[:-3]
        self.function = func
        self.output_path = output_path
        self.get_code()
        self.extract_dot()

    def get_code(self):
        sys.path.append(self.input_path)
        exec("from {a} import {b}".format(a=self.file, b=self.function))
        self.source = inspect.getsource(
            eval("{a}".format(a=self.function)))
        return (self)

    def show_graph(self):
        show_graph(self.source)

    def extract_pdf(self):
        extract_pdf(self.source, self.output_path)

    def extract_dot(self):
        extract_dot(self.source, self.output_path)
