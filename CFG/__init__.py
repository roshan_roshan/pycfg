from .ControlFlow import *
from .tool import *
from .usage import *
import os
import platform

if platform.system() == "Windows":
	os.environ["PATH"] += os.pathsep + "C:/Program Files (x86)/Graphviz/bin"