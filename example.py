from CFG import cfg

example = cfg('./example', "lcm.py", "compute", "./output")
example.extract_pdf()
example.extract_dot()
example.show_graph()
